package com.epam.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.app.model.Associates;

public interface Repository extends JpaRepository<Associates, Integer>{

	List<Associates> findByGender(String gender);

	boolean existsByName(String name);

}
