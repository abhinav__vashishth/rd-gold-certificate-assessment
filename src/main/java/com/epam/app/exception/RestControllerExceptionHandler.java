package com.epam.app.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class RestControllerExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request) {
		List<String> errorsList = new ArrayList<>();
		exception.getAllErrors().forEach(error -> errorsList.add(error.getDefaultMessage()));
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.name(),
				errorsList.toString(), request.getDescription(false));
	}

	@ExceptionHandler(AssociateException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleAssociateException(AssociateException exception, WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.name(),
				exception.getMessage(), request.getDescription(false));
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleDataIntegrityException(DataIntegrityViolationException exception, WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.name(),
				exception.getMessage(), request.getDescription(false));
		
	}
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleRuntimeException(RuntimeException exception, WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.name(),
				exception.getMessage(), request.getDescription(false));
		
	}
}