package com.epam.app.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ExceptionResponse {

	String timestamp;
	String status;
	String error;
	String path;
}
