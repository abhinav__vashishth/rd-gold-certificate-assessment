package com.epam.app.constant;

public class Constants {
	
	private Constants() {
	}

	public static final String BATCH_NOT_FOUND = "Batch Not Found";
	public static final String ASSOCIATE_NOT_FOUND = "Associate Not Found";
	public static final String ASSOCIATE_NAME_ALREADY_USED = "Associate Already Registered";
}
