package com.epam.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.app.dto.AssociatesDto;
import com.epam.app.service.Service;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rd/associates")
public class AssociateRestController {
	
	@Autowired
	Service service;

	@PostMapping
	public ResponseEntity<AssociatesDto> addAssociates(@RequestBody @Valid AssociatesDto associateDto){
		log.info("Entered Into Add Associates Rest Controller");
		return new ResponseEntity<>(service.addAssociate(associateDto),HttpStatus.CREATED);
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociatesDto>> getByGender (@PathVariable String gender){
		log.info("Entered Into Get Associate By Gender Rest Controller");
		return new ResponseEntity<>(service.getAssociatesByGender(gender),HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<AssociatesDto> updateAssociates(@RequestBody @Valid AssociatesDto associatesDto){
		log.info("Entered Into Update Associates Rest Controller");
		return new ResponseEntity<>(service.updateAssociate(associatesDto),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAssociatesById(@PathVariable int id){
		log.info("Entered Into Delete Associates By Id Rest Controller");
		service.deleteAssociateById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	

}
