package com.epam.app.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Batches {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name",nullable = false,unique = true)
	private String name;
	
	@Column(name = "practice",nullable = false,unique = true)
	private String practice;
	
	@Column(name = "startDate",nullable = false)
	private LocalDate startDate;
	
	@Column(name = "endDate",nullable = false)
	private LocalDate endDate;
	
	
}
