package com.epam.app.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Associates {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	@Column(name="name",unique = true,nullable = false)
	private String name;
	
	@Column(name="email",unique = true,nullable = false)
	private String email;
	
	@Column(name="gender",nullable = false)
	private String gender;
	
	@Column(name="college",nullable = false)
	private String college;
	
	@Column(name="status",nullable = false)
	private String status;
	
	@ManyToOne
	@JoinColumn(name="batch_id",referencedColumnName = "id")
	@Cascade(CascadeType.ALL)
	private Batches batch;

}
