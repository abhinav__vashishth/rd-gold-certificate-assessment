package com.epam.app.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.app.constant.Constants;
import com.epam.app.dto.AssociatesDto;
import com.epam.app.exception.AssociateException;
import com.epam.app.model.Associates;
import com.epam.app.repository.Repository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@org.springframework.stereotype.Service
public class Service {

	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	Repository repository;
	
	public AssociatesDto addAssociate(AssociatesDto associatesDto) {
		log.info("Entered into Add Associate Service");
		if(repository.existsByName(associatesDto.getName())) {
			log.error(Constants.ASSOCIATE_NAME_ALREADY_USED);
			throw new AssociateException(Constants.ASSOCIATE_NAME_ALREADY_USED);
		}
		Associates associate = modelMapper.map(associatesDto,Associates.class);
		return modelMapper.map(repository.save(associate), AssociatesDto.class);
	}

	public void deleteAssociateById(int id) {
		log.info("Entered into Delete Associate By Id Service");
		if(isAssociateExistsById(id)) {
			log.error(Constants.ASSOCIATE_NOT_FOUND);
			throw new AssociateException(Constants.ASSOCIATE_NOT_FOUND);
		}
		repository.deleteById(id);
		
	}


	public AssociatesDto updateAssociate(AssociatesDto associatesDto) {
		log.info("Entered into Update Associate Service");
		if(isAssociateExistsById(associatesDto.getId())) {
			log.error(Constants.ASSOCIATE_NOT_FOUND);
			throw new AssociateException(Constants.ASSOCIATE_NOT_FOUND);
		}
		Associates associates = modelMapper.map(associatesDto, Associates.class);
		return modelMapper.map(repository.save(associates),AssociatesDto.class);
	}

	public List<AssociatesDto> getAssociatesByGender(String gender) {
		log.info("Entered into Get Associate By Gender Service");
		return repository.findByGender(gender).stream()
				.map(a -> {
				AssociatesDto associatesDto= modelMapper.map(a, AssociatesDto.class);
				associatesDto.setBatch(a.getBatch());
				return associatesDto;
				})
				.toList();
	}
	

	private boolean isAssociateExistsById(int id) {
		return !repository.existsById(id);
	}
}
