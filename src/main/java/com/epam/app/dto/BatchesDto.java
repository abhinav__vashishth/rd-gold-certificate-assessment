package com.epam.app.dto;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BatchesDto {

	private int id;
	
	@NotBlank(message = "Invalid Batch Name")
	private String name;
	
	@NotBlank(message = "Invalid Practice Name")
	private String practice;
	
	@DateTimeFormat
	@NotBlank(message = "Start Date Invalid")
	private LocalDate startDate;
	
	@DateTimeFormat
	@NotBlank(message = "End Date Invalid")
	private LocalDate endDate;
}
