package com.epam.app.dto;

import com.epam.app.model.Batches;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AssociatesDto {

	int id;
	
	@NotBlank(message = "Invalid Name")
	private String name;
	
	@Email(message = "Invalid Email")
	private String email;

	@NotBlank(message = "Invalid gender ,Type : M/F")
	private String gender;
	
	@NotBlank(message = "Invalid college")
	private String college;
	
	@NotBlank(message = "Invalid status , Type : Active/Inactive/etc")
	private String status;

	@NotNull(message = "Invalid batch")
	private Batches batch;

}
