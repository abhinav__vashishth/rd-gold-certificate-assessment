package com.epam.app.controller.tests;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.app.controller.AssociateRestController;
import com.epam.app.dto.AssociatesDto;
import com.epam.app.exception.AssociateException;
import com.epam.app.model.Associates;
import com.epam.app.model.Batches;
import com.epam.app.service.Service;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateRestController.class)
class RestControllerTest {

	@MockBean
	Service service;
	
	@Autowired
	MockMvc mockMvc;
	
	Associates associate;
	AssociatesDto associateDto;
	Batches batch = new Batches();

	@BeforeEach
	void beforeEach() {
		associate = new Associates(1, "abhi", "abhi@gmail.com","M","Chitkara","Active" ,batch);
		associateDto = new AssociatesDto(1, "abhi", "abhi@gmail.com","M","Chitkara","Active" ,batch);

	}
	
	@Test
    void testAddAssociate() throws Exception {

        when(service.addAssociate(any(AssociatesDto.class))).thenReturn(associateDto);

        mockMvc.perform(post("/rd/associates")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(associateDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("abhi"));

        verify(service, times(1)).addAssociate(any(AssociatesDto.class));
    }
	
	@Test
	void testDeleteAssociate() throws Exception {

		int id=1;
		associateDto.setId(id);
		
		doNothing().when(service).deleteAssociateById(id);
		mockMvc.perform(delete("/rd/associates/{id}",id).contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isNoContent());

		verify(service).deleteAssociateById(id);

	}
	
	@Test
    void testUpdateAssociate() throws Exception {
 

        when(service.updateAssociate(any(AssociatesDto.class))).thenReturn(associateDto);

        mockMvc.perform(put("/rd/associates")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(associateDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("abhi"));

        verify(service, times(1)).updateAssociate(any(AssociatesDto.class));
    }
	
	@Test
	void testPostASsociateThrowMethodArgumentNotValidException() throws Exception {
		
		AssociatesDto associatesDto = new AssociatesDto();
		mockMvc.perform(post("/rd/associates")
		.contentType(MediaType.APPLICATION_JSON)
		.content(new ObjectMapper().writeValueAsString(associatesDto)))
		.andExpect(status().isBadRequest());;
		
	}
	
	@Test
	void testGetAssociateByGenderThrowsDataIntegrityViolationException() throws Exception {

		when(service.getAssociatesByGender("M")).thenThrow(new DataIntegrityViolationException("Data Integrity exception"));
		mockMvc.perform(get("/rd/associates/{gender}","M").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isBadRequest());


	}
	
	@Test
	void testGetAssociateByGenderThrowsRuntimeException() throws Exception {

		when(service.getAssociatesByGender("M")).thenThrow(new RuntimeException("Runtime exception"));
		mockMvc.perform(get("/rd/associates/{gender}","M").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isBadRequest());


	}
	
	@Test
	void testGetAssociateByGenderThrowsAssociateException() throws Exception {

		when(service.getAssociatesByGender("M")).thenThrow(new AssociateException("Associate exception"));
		mockMvc.perform(get("/rd/associates/{gender}","M").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isBadRequest());


	}
	
	@Test
	void testGetAssociateByGender() throws Exception {

		List<AssociatesDto> listOfAssociatesDto = new ArrayList<>();
		when(service.getAssociatesByGender("M")).thenReturn(listOfAssociatesDto);
		mockMvc.perform(get("/rd/associates/{gender}","M").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isOk());

		verify(service).getAssociatesByGender("M");

	}
}