package com.epam.app.service.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.app.constant.Constants;
import com.epam.app.dto.AssociatesDto;
import com.epam.app.dto.BatchesDto;
import com.epam.app.exception.AssociateException;
import com.epam.app.exception.ExceptionResponse;
import com.epam.app.model.Associates;
import com.epam.app.model.Batches;
import com.epam.app.repository.Repository;
import com.epam.app.service.Service;

@ExtendWith(MockitoExtension.class)
class ServiceTest {
	
	@Mock
	ModelMapper mapper;
	
	@InjectMocks
	Service service;
	
	@Mock
	Repository repository;
	
	AssociatesDto associateDto;
	Associates associate;
	Batches batch = new Batches();
	BatchesDto batchDto = new BatchesDto();

	@BeforeEach
	void beforeEach() {
		associate = new Associates();
		associate.setId(1);
		associate.setName("abhi");
		associate.setEmail("abhi@gmail.com");
		associate.setGender("M");
		associate.setCollege("Chitkara");
		associate.setStatus("Active");
		associate.setBatch(null);
		associate.getId();
		associate.toString();
		batch.toString();
		
		associateDto = new AssociatesDto();
		associateDto.setId(1);
		associateDto.setName("abhi");
		associateDto.setEmail("abhi@gmail.com");
		associateDto.setGender("M");
		associateDto.setCollege("Chitkara");
		associateDto.setStatus("Active");
		associateDto.getId();
		associateDto.setBatch(null);
		associateDto.toString();
		
		batchDto.setId(0);
		batchDto.setName(null);
		batchDto.setPractice(null);
		batchDto.setStartDate(null);
		batchDto.setEndDate(null);
		batchDto.getId();
		batchDto.toString();
		
		ExceptionResponse exceptionResponse = new ExceptionResponse(null, null, null, null);
		exceptionResponse.setPath(null);
		exceptionResponse.setError(null);
		exceptionResponse.setStatus(null);
		exceptionResponse.setTimestamp(null);

	}
	
	@Test
	void testAddAssociate() {
		when(mapper.map(associateDto, Associates.class)).thenReturn(associate);
		when(repository.save(associate)).thenReturn(associate);
		when(mapper.map(associate, AssociatesDto.class)).thenReturn(associateDto);
		AssociatesDto result = service.addAssociate(associateDto);
		assertEquals(result, associateDto);
		verify(mapper).map(associateDto, Associates.class);
		verify(mapper).map(associate, AssociatesDto.class);
		verify(repository).save(associate);
	}
	
	@Test
	void testDeleteAssociate() {
		
		when(repository.existsById(anyInt())).thenReturn(true);
		doNothing().when(repository).deleteById(anyInt());
		service.deleteAssociateById(5);
		verify(repository).existsById(5);
		verify(repository).deleteById(5);
	}

	@Test
	void testUpdateAssociate() {
		when(repository.existsById(anyInt())).thenReturn(true);
		when(mapper.map(associateDto, Associates.class)).thenReturn(associate);
		when(repository.save(associate)).thenReturn(associate);
		when(mapper.map(associate, AssociatesDto.class)).thenReturn(associateDto);
		AssociatesDto result = service.updateAssociate(associateDto);
		assertEquals(associateDto, result);
		verify(repository).existsById(1);
		verify(mapper).map(associateDto, Associates.class);
		verify(repository).save(associate);
		verify(mapper).map(associate, AssociatesDto.class);
	}
	
	@Test
	void testUpdateAssociateNotFoundFail() {
		when(repository.existsById(anyInt())).thenReturn(false);
		assertThrows(AssociateException.class, () -> service.updateAssociate(associateDto));
	}
	
	@Test void testGetByGenderReturnsAssociateDtoList() {
		String gender = "M";
		List<Associates> associateList = new ArrayList<>();
		associateList.add(associate);
		List<AssociatesDto> expectedDtoList = new ArrayList<>();
		expectedDtoList.add(associateDto);
		Mockito.when(repository.findByGender(gender)).thenReturn(associateList);
		Mockito.when(mapper.map(associate, AssociatesDto.class)).thenReturn(associateDto);
		List<AssociatesDto> result = service.getAssociatesByGender(gender);
		assertEquals(expectedDtoList.size(), result.size());
		assertEquals(expectedDtoList, result);
		Mockito.verify(repository, Mockito.times(1)).findByGender(gender);
		Mockito.verify(mapper, Mockito.times(1)).map(associate, AssociatesDto.class);
		Mockito.verify(mapper, Mockito.times(1)).map(associate, AssociatesDto.class);
		}
	
	@Test
	void testAddAssociateNameAlreadyUsedFail() {
		when(repository.existsByName(associateDto.getName())).thenReturn(true);
		AssociateException exception = assertThrows(AssociateException.class, ()->service.addAssociate(associateDto));
		assertEquals(Constants.ASSOCIATE_NAME_ALREADY_USED,exception.getMessage());
		verify(repository).existsByName(associateDto.getName());
		
	}
	@Test
	void testDeleteAssociateNotFoundFail() {
		when(repository.existsById(anyInt())).thenReturn(false);
		assertThrows(AssociateException.class, () -> service.deleteAssociateById(1));
		verify(repository).existsById(anyInt());
	}
}